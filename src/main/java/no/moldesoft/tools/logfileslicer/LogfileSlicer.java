package no.moldesoft.tools.logfileslicer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class LogfileSlicer {
    private Pattern pattern;
    private Path source;
    private Path destination;
    private Predicate<String> predicate;
    private Charset charset;
    private boolean not;

    public static void main(String[] args) {
        try {
            new LogfileSlicer().run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void run(String[] args) throws IOException {
        String[] regularArgs = getOptions(args);
        switch (regularArgs.length) {
            case 3:
                parseAndSetRegex(regularArgs[2]);
            case 2:
                if (pattern == null) {
                    parseAndSetRegex(null);
                }
                if (sliceInit(regularArgs[0], regularArgs[1])) {
                    processFile();
                }
                break;
            default:
                help();
                break;
        }
    }

    private String[] getOptions(String[] args) {
        List<String> noOpts = new ArrayList<>();
        Map<String, String> opts = new HashMap<>();
        Iterator<String> iterator = Arrays.asList(args).iterator();
        while (iterator.hasNext()) {
            String arg = iterator.next();
            if (arg.startsWith("-")) {
                int typeOfOpt = arg.startsWith("--") ? 2 : 1;
                String opt = arg.substring(typeOfOpt);
                String value = null;
                if (opt.contains("=")) {
                    int posOfEquals = opt.indexOf('=');
                    value = opt.substring(posOfEquals + 1).trim();
                    opt = opt.substring(0, posOfEquals).trim();
                }
                if (value == null) {
                    if ("cs".equalsIgnoreCase(opt) || "charset".equalsIgnoreCase(opt)) {
                        if (typeOfOpt == 1 && iterator.hasNext()) {
                            value = iterator.next();
                        }
                    } else if ("not".startsWith(opt.toLowerCase()) && opt.length() <= 3) {
                        not = true;
                    }
                }
                if (value != null) {
                    opts.putIfAbsent(opt.toLowerCase(), value);
                }
            } else {
                noOpts.add(arg);
            }
        }
        charset = Charset.forName(opts.getOrDefault("cs", opts.getOrDefault("charset", "ISO-8859-1")));
        return noOpts.toArray(new String[0]);
    }

    private boolean sliceInit(String srcPath, String destPath) throws IOException {
        boolean canProceed = true;

        source = Paths.get(srcPath);
        if (Files.exists(source) && Files.isRegularFile(source) && Files.isReadable(source)) {
            System.out.println("Reading from  " + source);
            if (charset != null) {
                System.out.println("Using charset " + charset.displayName());
            }
            if (pattern != null) {
                System.out.println("Using pattern " + pattern.toString());
                System.out.print(not ? "Rejecting" : "Accepting");
                System.out.println(" records matching the pattern");
            }
        } else {
            canProceed = false;
            System.out.println("Source file can't be read.");
        }

        destination = Paths.get(destPath);
        if (!Files.exists(destination) ||
                Files.isRegularFile(destination) && Files.isWritable(destination) &&
                        !Files.isSameFile(source, destination)) {
            System.out.println("Writing to    " + destination);
            if (charset == null || !charset.equals(StandardCharsets.UTF_8)) {
                System.out.println("using charset " + StandardCharsets.UTF_8.displayName());
            }
        } else {
            canProceed = false;
            System.out.println("Destination file can't be used for writing.");
        }

        return canProceed;
    }

    private void processFile() throws IOException {
        Stats stats = new Stats();
        try (BufferedReader reader = Files.newBufferedReader(source, charset);
             BufferedWriter writer = Files.newBufferedWriter(destination, StandardOpenOption.CREATE)) {

            List<String> logEntry = new ArrayList<>();

            try {
                reader.lines().forEach(line -> {
                    stats.linesRead++;
                    if (startOfLogEntry(line)) {
                        processLogEntry(logEntry, writer, stats);
                    }
                    logEntry.add(line);
                });

                processLogEntry(logEntry, writer, stats);
            } catch (UncheckedIOException e) {
                throw e.getCause();
            }
        } finally {
            System.out.println(stats);
        }
    }

    private boolean startOfLogEntry(String line) {
        return !line.isEmpty() && !Character.isWhitespace(line.charAt(0));
    }

    private void processLogEntry(List<String> logEntry, BufferedWriter writer, Stats stats) {
        if (!logEntry.isEmpty()) {
            stats.entriesRead++;
            Stream<String> stream = logEntry.stream();
            boolean keep = not ? stream.noneMatch(predicate) : stream.anyMatch(predicate);
            if (keep) {
                logEntry.forEach(line -> {
                    try {
                        writer.write(line);
                        writer.newLine();
                        stats.linesWritten++;
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
                stats.entriesWritten++;
            }
            logEntry.clear();
        }
    }

    private void parseAndSetRegex(String arg) {
        if (arg != null && !arg.isEmpty()) {
            pattern = Pattern.compile(arg);
            predicate = pattern.asPredicate();
        } else {
            predicate = s -> true;
        }
    }

    private void help() {
        System.out.println();
        System.out.println("This program reads lines from a source file and prints it on a destination file.");
        System.out.println("It expects the source file as first argument, the destination as the second argument,");
        System.out.println("and, optionally a regex expression (java-style) as third argument.");
        System.out.println();
        System.out.println("The source file is expected to be a log-file, with each log entry starting from the first");
        System.out.println("position of the line, and possibly followed by extra lines starting with whitespace.");
        System.out.println();
        System.out.println("It looks for the regex expression in each line of a log entry, not only the first line.");
        System.out.println("If a line has a match, the entire log entry will be written to the target file.");
        System.out.println();
        System.out.println("Default charset for source file is ISO-8851-1.");
        System.out.println("Use -cs UTF-8 or --cs=UTF-8 to set charset of source to UTF-8.");
    }

    private static class Stats {
        long linesRead;
        long linesWritten;
        long entriesRead;
        long entriesWritten;

        @Override
        public String toString() {
            return "Lines read: " + linesRead +
                    "\nLines written: " + linesWritten +
                    "\nLog entries read: " + entriesRead +
                    "\nLog entries written: " + entriesWritten;
        }
    }
}
